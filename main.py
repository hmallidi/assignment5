#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

# -----------
# imports
# -----------

from flask import Flask, render_template, request, redirect, url_for, \
                  jsonify, Response
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]

missing_ids = set()
next_id = 4


@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return Response(r)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_title = request.form['title']

        element = None

        global next_id
        if len(missing_ids) == 0:
            element = {'title': new_title, 'id': str(next_id)}
            next_id += 1
        else:
            element = {'title': new_title, 'id': str(missing_ids.pop())}

        global books
        books.append(element)
        books = sorted(books, key=lambda i: i['id'])

        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    if request.method == 'POST':
        new_title = request.form['title']

        for book in books:
            if book['id'] == str(book_id):
                book['title'] = new_title

        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', book_id=book_id)


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for book in books:
            if book['id'] == str(book_id):
                books.remove(book)
                missing_ids.add(book_id)

        return redirect(url_for('showBook'))
    else:
        book_title = None
        for book in books:
            if book['id'] == str(book_id):
                book_title = book['title']

        return render_template('deleteBook.html', book_id=book_id,
                               book_title=book_title)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
