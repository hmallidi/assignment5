import googlemaps
import json
import time
import usaddress
import requests

cities_list = ['New York', 'Los Angeles', 'Chicago', 'Houston', 'Philadelphia',
               'Phoenix', 'San Diego', 'San Antonio', 'Dallas', 'San Jose',
               'Detroit', 'Jacksonville', 'Indianapolis', 'Columbus',
               'San Francisco', 'Austin', 'Memphis', 'Fort Worth', 'Baltimore',
               'Charlotte', 'El Paso', 'Milwaukee', 'Boston', 'Seattle',
               'Denver', 'Washington', 'Portland', 'Las Vegas',
               'Oklahoma City', 'Nashville', 'Tucson', 'Albuquerque',
               'Long Beach', 'Sacramento', 'Fresno', 'New Orleans', 'Mesa',
               'Cleveland', 'Virginia Beach', 'Kansas City', 'Atlanta',
               'San Juan', 'Omaha', 'Oakland', 'Honolulu', 'Miami', 'Tulsa',
               'Colorado Springs', 'Minneapolis', 'Arlington', 'Wichita',
               'Santa Ana', 'Raleigh', 'Anaheim', 'Tampa', 'Saint Louis',
               'Pittsburgh', 'Toledo', 'Cincinnati', 'Aurora', 'Riverside',
               'Bakersfield', 'Stockton', 'Corpus Christi', 'Newark',
               'Buffalo', 'Anchorage', 'Saint Paul', 'Lexington-Fayette',
               'Plano', 'Saint Petersburg', 'Norfolk', 'Louisville',
               'Lincoln', 'Glendale', 'Henderson', 'Jersey City', 'Chandler',
               'Greensboro', 'Birmingham', 'Fort Wayne', 'Scottsdale',
               'Hialeah', 'Madison', 'Baton Rouge', 'Chesapeake', 'Garland',
               'Modesto', 'Paradise', 'Chula Vista', 'Lubbock', 'Rochester',
               'Laredo', 'Akron', 'Orlando', 'Durham', 'Glendale', 'Fremont',
               'San Bernardino', 'Reno']


def get_city_opendata(city):
    tmp = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&q=%s&sort=population&facet=country&refine.country=%s'
    city = city.replace(" ", "+")
    cmd = tmp % (city, 'us')
    res = requests.get(cmd)
    dct = json.loads(res.content)
    out = dct['records'][0]['fields']

    out['city'] = out['accentcity']
    out['state'] = out['region']

    del out['country']
    del out['accentcity']
    del out['region']
    del out['geopoint']

    return out


def getQuery(city, state, name):
    name = name.replace(" ", "+")
    city = city.replace(" ", "+")

    if name == "":
        query = city + "+" + state
    else:
        query = name + "+in+" + city + "+" + state

    return query


def reformatLocation(location):
    if "geometry" in location and "location" in location["geometry"]:
        coordinates = location["geometry"]["location"]
        location["location"] = coordinates
        del location["geometry"]

    if "opening_hours" in location and "weekday_text" in location["opening_hours"]:
        weekday_text = location["opening_hours"]["weekday_text"]
        location["weekday_text"] = weekday_text
        del location["opening_hours"]

    try:
        parsed_address = usaddress.tag(location["formatted_address"])[0]

        location['zip_code'] = parsed_address['ZipCode']
    except (usaddress.RepeatedLabelError, KeyError):
        return None

    return location


def getJSON(place_type, city, state, name="", min_price=0, max_price=4):
    gmaps = googlemaps.Client(key='AIzaSyDzN3W-S4TxBm3wqslV1_JqfwhLjEsSKI8')

    city = city.strip()
    name = name.strip()

    page_token = ""
    places_results = list()
    query = getQuery(city, state, name)

    while True:
        places_results_next_page = None

        if place_type == "restaurant":
            places_results_next_page = gmaps.places(query=query,
                                                    min_price=min_price,
                                                    max_price=max_price,
                                                    type=place_type,
                                                    page_token=page_token)
        else:
            places_results_next_page = gmaps.places(query=query,
                                                    type=place_type,
                                                    page_token=page_token)

        for location in places_results_next_page["results"]:

            # check if place_id is already in database here

            new_place_results = gmaps.place(place_id=location["place_id"],
                                            fields=('business_status',
                                                    'formatted_address',
                                                    'formatted_phone_number',
                                                    'geometry', 'name',
                                                    'opening_hours',
                                                    'place_id', 'price_level',
                                                    'rating', 'url',
                                                    'website',))
            location = new_place_results["result"]

            location = reformatLocation(location)

            if location is not None:
                location['city'] = city
                location['state'] = state
                location['place_type'] = place_type
                places_results.append(location)

        time.sleep(2)

        if "next_page_token" in places_results_next_page:
            page_token = places_results_next_page["next_page_token"]
        else:
            break

    return places_results


def getPlaceDetails(place_id):
    gmaps = googlemaps.Client(key='AIzaSyDzN3W-S4TxBm3wqslV1_JqfwhLjEsSKI8')

    new_place_results = gmaps.place(place_id=place_id,
                                    fields=('business_status',
                                            'formatted_address',
                                            'formatted_phone_number',
                                            'geometry', 'name',
                                            'opening_hours',
                                            'place_id', 'price_level',
                                            'rating', 'url',
                                            'website',))

    return new_place_results["result"]


# city = 'Anaheim'
# city_result = get_city_opendata(city)
# state = city_result['state']
# places_results = getJSON("hospital", city, state)
# places_results.extend(getJSON("drugstore", city, state))

# LOCATIONS DICT
#   Keys/Info
#       place_id - primary key (string)
#
#       name - (string)
#       place_type - (string)
#
#       formatted_address - (string)
#       city - (string)
#       state_abbr - (string)
#       zip_code - (string)
#       location - (array of latitude and longitude)
#
#       business_status - (string)
#       opening_hours - (array of strings)
#       price_level - int
#       rating - double
#
#       url - string
#       website - string

# CITY DICT
#   Keys/Info
#       city - string
#       state_abbr - string
#
#       longitude - float
#       latitude - float
#
#       population - int


# print(city_result)
# print()
# print()
# for location in places_results:
#     print(location['name'] + " " + location['place_type'])
#     print(location['city'] + "  " + location['zip_code'])
#     print()
