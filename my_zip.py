def my_zip(*args):
    """
    Zip function with any number of iterables as parameters.

    returns generator
    doesn't assume indexable iterables
    only makes one pass through iterables (doesn't use len() function)
    """

    zipped_list = list()

    try:
        # iterating through this iterable in args once only
        for element in args[0]:
            zipped_list.append([element])
    except IndexError:
        yield from ()  # if args is empty

    args_index = 1

    while True:
        try:
            args_element_index = 0

            # iterating through the rest of the iterables in args once only
            for element in args[args_index]:
                zipped_list[args_element_index].append(element)
                args_element_index += 1
            args_index += 1
        except IndexError:  # once we reach the end of args
            break

    for zip_element in zipped_list:
        yield tuple(zip_element)
